**Key-Croc-AP**

This howto will describe the steps needed to use an external USB WiFi adapter as an access point on the Hak5 Key Croc. Why? Well, not sure why really. Just wanted to try to get it working. And, there might be situations where you want to use the Croc in other ways than originally intended and be able to access the Croc by connecting to it via a wireless access point and also use the onboard WiFi interface as a client/STA at the same time to get internet access. It will not forward any traffic from any client device that is connected to the AP. This can be solved though by configuring forwarding and set some iptables rules. According to tests so far, creating an AP this way will "break" the original functionality of attaching a keyboard and logging keystrokes so the Croc will not work as a key logger with this setup.

And, as always, any tinkering is made at your own risk. If things break, it's your responsibility. You have been warned... :-)

First, you need to find a USB WiFi adapter. Using an adapter in this howto that has the same type of chipset as the onboard radio, an 8188FTV (vid:pid = 0bda:f179).

But, even if using an adapter with the same type of chipset (with the correct driver available in the system), it will still not show up when running ifconfig. Executing lsusb will show it though.

So, when attaching the USB WiFi adapter, the directory 2-1 (and subdirs) is made available in the path below. Note that other directory names might be used. Just tested this on one Croc, so not sure if it differs between devices. The qualified guess is that it won't.

```/sys/devices/platform/sunxi-ehci.3/usb2/```

The adapter should be enumerated under

```/sys/devices/platform/sunxi-ehci.3/usb2/2-1/2-1:1.0```

However, that won't tell what the actual interface name is, but it's available a bit further down the tree.
The following path should contain the interface name, such as wlxc065ec6fc458 (this is an example device name, use the one specific to the system at hand).

```/sys/devices/platform/sunxi-ehci.3/usb2/2-1/2-1:1.0/net/```

The following path will also show available network interfaces

```/sys/class/net```

Running cat on this file should also show some relevant info

```cat /proc/net/dev```


OK, the interface name is known by now, so lets bring that interface up

```ifconfig wlxc065ec6fc458 up```

Execute ifconfig again and it will show as a WiFi interface on the Croc

With the interface identified and active, it's possible to continue with the next steps of setting up the AP and DHCP

Install hostapd and udhcpd (the latter if you want DHCP, not mandatory but convenient)
```
apt update
apt install hostapd
apt install udhcpd
```

**NOTE! DO NOT USE DNSMASQ !!!**
There is some bug in dnsmasq that has been fixed in newer versions but the package in the Debian repos available to the Croc contains a version that has a bug present that will produce the "junk found in command line" error when running the service... AVOID!!!

Set a static IP address for the USB WiFi adapter (IP addresses can be from whatever private IP address range, the important thing is that the range matches in the different setup steps below)

```nano /etc/dhcpcd.conf```

add:

```
interface wlxc065ec6fc458

static ip_address=10.64.128.1

static domain_name_servers=1.1.1.1 8.8.8.8

nohook wpa_supplicant
```

and create a file with the same name as the WiFi interface (for example: wlxc065ec6fc458) in the /etc/network/interfaces.d/ directory with the contents

```
allow-hotplug wlxc065ec6fc458

auto wlxc065ec6fc458

iface wlxc065ec6fc458 inet static

         address 10.64.128.1

         netmask 255.255.255.0

         gateway 10.64.128.1

         dns-nameserver 1.1.1.1

         dns-nameserver 8.8.8.8
```

Either of the above could be enough, but do both. 

Now edit the udhcpd configuration

```cp /etc/udhcpd.conf /etc/udhcpd.conf.org```

```nano /etc/udhcpd.conf```

change the following parameters:

```
start 10.64.128.100
end 10.64.128.109
interface wlxc065ec6fc458

uncomment "lease_file" but let the path remain default

opt     dns     1.1.1.1 8.8.8.8
option  subnet  255.255.255.0
opt     router  10.64.128.1
option  lease   864000          # 10 days of seconds
```


Enable udhcpd

```nano /etc/default/udhcpd```

change


```DHCPD_ENABLED="no"```

to

```DHCPD_ENABLED="yes"```

Edit the settings for hostapd

```nano /etc/hostapd/hostapd.conf```

Add something like this:

```
interface=wlxc065ec6fc458
driver=nl80211
ssid=CrocBite
country_code=(enter a relevant 2 letter country code)
hw_mode=g
channel=11
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=hak5croc-ap
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP
wpa_group_rekey=86400
ieee80211n=1
wmm_enabled=1
```

Edit the following file

```nano /etc/default/hostapd```

Find the line

```#DAEMON_CONF=""```

and edit it so that it says

```DAEMON_CONF="/etc/hostapd/hostapd.conf"```

Don't forget to remove the # in the beginning of the line to activate it!

Likewise, run ```nano /etc/init.d/hostapd```
and find the line

```DAEMON_CONF=```

and change it to

```DAEMON_CONF=/etc/hostapd/hostapd.conf```

Note that the first file uses quotation marks, the second doesn't

Start up the udhcpd and hostapd services again
```
systemctl daemon-reload
systemctl start hostapd
systemctl start udhcpd
```

If you get an error like "unit hostapd.service is masked", run the following commands before starting the hostapd service
```
systemctl unmask hostapd
systemctl enable hostapd
```

Check status
```
systemctl status hostapd
systemctl status udhcpd
```

iwconfig isn't available by default on the Croc (makes it a bit easier to check wireless things but not mandatory), install it with
```apt install wireless-tools```


In order to autostart the AP and DHCP daemon as well as bringing up the WiFi interface on boot, simply make use of the croc_framework file. There are other ways, this is just one of them.

In
```/usr/local/croc/bin/croc_framework```
add
```/root/start-ap.sh &```
before "chomp" at the bottom the file

Create the ```/root/start-ap.sh``` file with the following content, then make it executable. The bash file is also available in this repo.


```
#!/bin/bash

# Replace interface name with one relevant to the system at hand
ifconfig wlxc065ec6fc458 up
sleep 5
systemctl restart hostapd
sleep 2
systemctl restart udhcpd
```

Restart the Croc and verify. It should now be possible to see the ESSID when looking for available APs and also connect to it.

It might be good to be able to use preconfigured access points as a client instead of the one that is statically configured in the ```config.txt``` file of the Croc. So, if connecting to the Croc in attack mode using the Croc AP as described above, then it's possible to store several different AP setups and activate them manually. There's a bash script (alt-ap.sh) added to this repo that can help with that.
