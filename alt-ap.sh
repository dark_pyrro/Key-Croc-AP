#!/bin/bash

# A script that helps to connect to an alternative AP when
# the Croc is in arming or attack mode.
# The alternative wpa_supplicant config file has to be
# created before running this script.
# This makes it possible to have several alternative
# config files stored and use them as desired depending
# on what AP is available.
#
# Usage:
# ./alt-ap.sh <full path to alternative wpa_supplicant config file>
#
# Example:
# ./alt-ap.sh /etc/wpa_supplicant.conf-alt01


# Check that not more than one (1) command line parameter/argument was passed to the script
if [ $# -gt 1 ]
  then
    echo "Too many arguments passed to the script. Only one (1) allowed. Exiting..."
    exit
fi

# Checking that an argument has been passed to the script
if [ -z "$1" ]
  then
    echo "No wpa config file name supplied. Exiting..."
    exit
fi

# Checking that the WiFi config file exists
if [ -f "$1" ]; then
    echo "Using: $1"
else 
    echo "$1 does not exist. Use full/absolute path to the file. Exiting..."
    exit
fi

# Get PIDs for already running processes
PID_WPA=$(pidof wpa_supplicant)
PID_DHC=$(pidof dhclient)

# Killing any existing wpa_supplicant process
if [ -z "$PID_WPA" ];
then
    echo "No wpa_supplicant process to kill, continuing..."
else
    echo "Killing existing wpa_supplicant process: $PID_WPA"
    kill -9 $PID_WPA
    sleep 1
fi

# Killing any existing dhclient process
if [ -z "$PID_DHC" ];
then
    echo "No dhclient process to kill, continuing..."
else
    echo "Killing existing dhclient process: $PID_DHC"
    kill -9 $PID_DHC
    sleep 1
fi

# Start new processes in order to connect to the desired AP
sleep 2
wpa_supplicant -Dnl80211 -iwlan0 -c$1 -B &
sleep 2
dhclient wlan0 &
