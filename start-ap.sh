#!/bin/bash

# Replace interface name with one relevant to the system at hand
ifconfig wlxc065ec6fc458 up
sleep 5
systemctl restart hostapd
sleep 2
systemctl restart udhcpd
